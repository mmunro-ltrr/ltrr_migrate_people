<?php
/**
 * Makes UAQS Person content from the old People nodes in the LTRR directory.
 */
class LtrrMigratePeoplePersonMigration extends DrupalNode7Migration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Documented lists of source data fields:.
    $fields = array(
      'active_flag' => t('Active at LTRR == 1'),
      'address_combined' => t('Room locations'),
      'body' => t('Interests'),
      'field_person_connection' => t('Connection'),
      'field_person_closeup' => t('Closeup image'),
      'field_person_first_name' => t('First Name'),
      'field_person_job_category' => t('Category'),
      'field_person_job_description' => t('Job Description'),
      'field_person_job_title' => t('Title'),
      'field_person_last_name' => t('Last Name'),
      'field_person_middle_name' => t('Middle Name'),
      'field_person_nickname' => t('Nickname'),
      'field_person_photo' => t('Photo'),
      'field_person_primary_email' => t('Email'),
      'field_person_primary_phone' => t('Phone'),
      'field_person_primary_room' => t('Room'),
      'field_person_reports_to' => t('Reports To'),
      'field_person_secondary_email' => t('Other Email'),
      'field_person_secondary_phone' => t('Other Phone'),
      'field_person_secondary_room' => t('Other Room'),
      'field_person_webpage' => t('Webpage'),
      'field_person_work_status' => t('Status'),
      'field_uid' => t('UA NetID'),
      'image_alt_text' => t('Photo alt text (derived from other fields)'),
      'job_titles' => t('Job Title(s)'),
      'path' => t('URL path settings'),
      'phones' => t('Phone Number(s)'),
      'title' => t('Title'),
    );

    $image_src_field = 'field_person_closeup';

    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('status', 'active_flag');

    // Direct field mappings.
    $this->addFieldMapping('field_uaqs_lname', 'field_person_last_name');
    $this->addFieldMapping('field_uaqs_fname', 'field_person_first_name');
    $this->addFieldMapping('field_uaqs_email', 'field_person_primary_email');
    $this->addFieldMapping('field_uaqs_bio', 'body');

    // Derived field mappings.
    $this->addFieldMapping('field_uaqs_addresses', 'address_combined');
    $this->addFieldMapping('field_uaqs_phones', 'phones');
    $this->addFieldMapping('field_uaqs_titles', 'job_titles');
    $this->addFieldMapping('field_uaqs_person_category', 'category_term');

    // Image fields are more complicated.
    $image_dst_field = 'field_uaqs_photo';
    $this->addFieldMapping($image_dst_field, $image_src_field);
    $this->addFieldMapping($image_dst_field . ':file_class')
         ->defaultValue('MigrateFileFid');
    $this->addFieldMapping($image_dst_field . ':preserve_files')
         ->defaultValue(TRUE);
    $this->addFieldMapping($image_dst_field . ':alt', 'image_alt_text');
    $this->addFieldMapping($image_dst_field . ':title', 'image_alt_text');
    $this->addFieldMapping($image_dst_field . ':width', $image_src_field . ':width');
    $this->addFieldMapping($image_dst_field . ':height', $image_src_field . ':height');

    // Allow limited HTML markup in the bio field.
    $this->addFieldMapping('field_uaqs_bio:format')
         ->defaultValue('uaqs_unconstrained');
  }

  /**
   * Implementation of prepareRow(), to concatenate some raw field values.
   *
   * See the Migration migrate_example code, in particular wine.inc.
   *
   * @param object $row
   *   Object containing raw source data (from the old nodes).
   *
   * @return bool
   *   TRUE to process this row, FALSE to have the source skip it.
   */
  public function prepareRow($row) {
    // Recommended boilerplate section: invoke the parent classes on the
    // current row first, and pass on their status (whether to process
    // or skip the row).
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Translate the multiple codes for degrees of connection.
    $row->active_flag = 0;
    if (isset($row->field_person_connection) && ($row->field_person_connection[0] < 50)) {
      $row->active_flag = 1;
    }

    // Physical address concatenation.
    if (isset($row->field_person_primary_room)) {
      if (isset($row->field_person_secondary_room)) {
        $row->address_combined = array(implode("\n", array(
          $row->field_person_primary_room[0],
          $row->field_person_secondary_room[0],
        )));
      }
      else {
        $row->address_combined = $row->field_person_primary_room;
      }
    }

    // Pair of phone fields to one multi-value field.
    if (isset($row->field_person_primary_phone)) {
      $row->phones = array($row->field_person_primary_phone);
      if (isset($row->field_person_secondary_phone)) {
        $row->phones[] = $row->field_person_secondary_phone;
      }
    }

    // Job titles from taxonomy term lookup.
    if (isset($row->field_person_job_description)) {
      $tquery = db_select('taxonomy_term_data', 't');
      $tquery->join('taxonomy_vocabulary', 'v', 't.vid=v.vid');
      $tquery->condition('v.machine_name', 'positions')
        ->condition('t.tid', $row->field_person_job_description[0])
        ->fields('t', array('name'));
      $row->job_titles = $tquery->execute()->fetchCol(0);
    }

    // Job category re-coding.
    $category_term_lookup = array(
      10 => 'Director',
      20 => 'Associate Director',
      30 => 'LTRR Faculty',
      40 => 'Adjunct Faculty',
      50 => 'Joint Faculty',
      60 => 'Research Associates',
      70 => 'Emeritus Faculty',
      80 => 'Administrative Staff',
      90 => 'Staff, Scientific',
      100 => 'Graduate Students',
      110 => 'Temporary Employees',
      120 => 'Visitors and Consultants',
      130 => 'Undergraduates',
      140 => 'Docents',
      150 => 'Outreach',
    );
    if (isset($row->field_person_job_category)) {
      $job_category = $row->field_person_job_category[0];
      if (isset($category_term_lookup[$job_category])) {
        $row->category_term = array($category_term_lookup[$job_category]);
      }
    }

    // Concatenate names and suffix into one derived field.
    $row->image_alt_text = implode(' ', array(
      $row->field_person_first_name[0],
      $row->field_person_last_name[0],
      'portrait',
    ));
    return TRUE;
  }

}
